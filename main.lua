
score = 0
game_over = false
enemies_controller = {}
enemies_controller.enemies = {}
enemy = {}
enemies_controller.image = love.graphics.newImage('enemy.png')
function checkCollisions(enemies,bullets)
for i,e in pairs(enemies) do
	for _,b in pairs(bullets) do
	if b.y <= e.y + e.height and b.x > e.x and b.x < e.x + e.width then
		table.remove(enemies,i)
		score = score + 10
		table.remove(player.bullets,_)
	end
end
end
end

function check_player_hit()
	for _,b in pairs(enemy_bullets.bullets) do
	if b.y >= player.y - 80 and b.x > player.x and b.x < player.x + player.width then
		score = score - 1
		table.remove(enemy_bullets.bullets,_)

	end
end

end

function check_ingame(enemies)
for i,p in pairs(enemies) do
	if p.y >= player.y then
		table.remove(enemies,i)
		score = score - 2.5
	end
end

end
function love.load()
--width, height = love.graphics.getDimensions( )
background = love.graphics.newImage("map.png")
sound = love.audio.newSource("country.mp3", "stream")
sound:setVolume(0.33)
love.audio.play(sound)
love.window.setMode(1920,1080)	
width, height = love.graphics.getDimensions( )



end
enemy_bullets = {}
enemy_bullets.bullets = {}

--integrate enemy_shoot cooldown with love.update instead of the enemy_bullets 
function enemy_bullets.fire(x,y)
	bullet = {}
	bullet.x = x  
	bullet.y = y 
	table.insert(enemy_bullets.bullets,bullet)
end


	
player = {}
player.width = 300
player.height = 300
player.x = 720
player.y = 750
player.bullets = {}
player.cooldown = 20/60
player.speed = 30*60
player.image = love.graphics.newImage('player.png')
player.fire_sound = love.audio.newSource('Laser_Shoot2.wav','stream')
player.fire_sound:setVolume(.25)
player.fire = function()
	if player.cooldown <= 0 then
	love.audio.play(player.fire_sound)
	player.cooldown = 20
	bullet = {}
	bullet.x = player.x + 140
	bullet.y = player.y - 30
	table.insert(player.bullets,bullet)
	end	
	love.graphics.setDefaultFilter('nearest','nearest')

end


function wave()

	enemies_controller:spawnEnemy(math.random(0,width),math.random(0,150),math.random(10,150))
	enemies_controller:spawnEnemy(math.random(0,width),math.random(0,150),math.random(10,150))
	enemies_controller:spawnEnemy(math.random(0,width),math.random(0,150),math.random(10,150))
	enemies_controller:spawnEnemy(math.random(0,width),math.random(0,150),math.random(10,150))
	enemies_controller:spawnEnemy(math.random(0,width),math.random(0,150),math.random(10,150))
	enemies_controller:spawnEnemy(math.random(0,width),math.random(0,150),math.random(10,150))
end

function enemies_controller:spawnEnemy(x,y,cd)

enemy = {}
enemy.x = x
enemy.y = y
enemy.width = 100
enemy.height = 100
--enemy.bullets = {}
enemy.cooldown = cd
enemy.speed = 120
table.insert(enemies_controller.enemies,enemy)
end
function love.update(dt)
	if player.x > width then
    player.x = 0
else if player.x < -300 then
	player.x = width
	end

	player.cooldown = player.cooldown - 60 * dt
	check_player_hit()
if love.keyboard.isDown("right")then
	player.x = player.x + player.speed * dt
 
elseif love.keyboard.isDown("left")then
	player.x = player.x - player.speed * dt
end
if love.keyboard.isDown("space") then 
	player.fire()
end

for _,e in pairs(enemies_controller.enemies) do 
  e.y= e.y +e.speed * dt 
  e.cooldown = e.cooldown - 60 * dt

end
for _,enemy in pairs(enemies_controller.enemies) do 
if enemy.cooldown == 0 or enemy.cooldown >-10 and enemy.cooldown <0 then 
	enemy_bullets.fire(enemy.x,enemy.y)
  	elseif enemy.cooldown <= -15 then
  	enemy.cooldown = math.random(60,360)

  end
end
for i,b in pairs(player.bullets) do 
	b.y = b.y - 15*60 * dt
	if b.y < -2 then
		table.remove(player.bullets,i)

		end
		end
for i,buloc in pairs(enemy_bullets.bullets) do
	buloc.y = buloc.y + 5*60 * dt
	if buloc.y >= player.y + 300 then
		table.remove(enemy_bullets.bullets,i)
	end
end 


checkCollisions(enemies_controller.enemies,player.bullets)


check_ingame(enemies_controller.enemies)


if #enemies_controller.enemies <= 0 then
	wave()



end
end
end

function love.draw()
	
	love.graphics.draw(background)
	love.graphics.draw(player.image,player.x,player.y)
	-- draw the play next the enemies
	love.graphics.setColor(255,255,255)
	for _,e in pairs(enemies_controller.enemies) do
		love.graphics.draw(enemies_controller.image,e.x,e.y,0)
	end
	
	love.graphics.print(score)
	love.graphics.setColor(255,255,255)
	for _,b in pairs(player.bullets) do
		love.graphics.rectangle("fill",b.x,b.y,20,20)	
	end	
for _,cords in pairs(enemy_bullets.bullets) do
			love.graphics.rectangle("fill",cords.x+40,cords.y+80,20,20)	
		
end
end
